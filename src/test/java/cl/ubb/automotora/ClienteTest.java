package cl.ubb.automotora;
	
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.ClienteDao;
import cl.ubb.model.Cliente;
import cl.ubb.service.ClienteService;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ClienteTest {
	@Mock
	private ClienteDao clienteDao;
	
	@InjectMocks
	private ClienteService clienteService;
	
	@Test
	public void registrarClienteYRetornarElCliente(){

		//arrange
		
		Cliente cliente = new Cliente();
		cliente.setRut("19.333.384-2");
		cliente.setNombre("Juan Perez");
		cliente.setNumeroTel("983472618");
		cliente.setEmail("juan.perez@gmail.com");
		//act
		when(clienteDao.save(cliente)).thenReturn(cliente);
		Cliente clienteCreado = clienteService.crearCliente(cliente);
		
		//assert
		assertEquals(cliente,clienteCreado);
		
	}
	@Test
	public void ingresarRutDeClienteRetornarVerdaderoSiEstaRegistrado(){
		//arrange
		Cliente cliente = new Cliente();
		cliente.setRut("9.413.269-k");
		
		//act
		when(clienteDao.exists(cliente.getRut())).thenReturn(true);
		boolean respuestaRetornada = clienteService.buscarCliente(cliente.getRut());
		
		
		//assert
		assertTrue(respuestaRetornada);
	}
	

	
}	