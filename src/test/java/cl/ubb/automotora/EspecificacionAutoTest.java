package cl.ubb.automotora;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.EspecificacionAutoDao;
import cl.ubb.model.EspecificacionAuto;
import cl.ubb.service.EspecificacionAutoService;
@RunWith(MockitoJUnitRunner.class)
public class EspecificacionAutoTest {
	@Mock
	private EspecificacionAutoDao especificacionAutoDao;
	
	@InjectMocks
	private EspecificacionAutoService especificacionAutoService;
	
	@Test
	public void ingresaIdentificadorDeAutoRetornaMarcaModeloYAnio() {
		//arrange
		EspecificacionAuto especificacionAuto = new EspecificacionAuto();
		especificacionAuto.setId(3L);
		
		EspecificacionAuto especificacionAutoRetornado = new EspecificacionAuto();
		
		//act
		when(especificacionAutoDao.findOne(3L)).thenReturn(especificacionAuto);
		especificacionAutoRetornado = especificacionAutoService.obtenerEspecificacionAuto(especificacionAuto.getId());
		
		
		assertNotNull(especificacionAutoRetornado);
		
		
	}

}
