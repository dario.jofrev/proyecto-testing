package cl.ubb.automotora;

import static org.junit.Assert.*;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.EspecificacionAutoDao;
import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Cliente;
import cl.ubb.model.Reserva;
import cl.ubb.service.EspecificacionAutoService;
import cl.ubb.service.ReservaService;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ReservaTest {
	@Mock
	private ReservaDao reservaDao;
	
	@InjectMocks
	private ReservaService reservaService;
	@Test
	public void reservarAutoIngresandoIdClienteFechaInicioYFechaFinRetornaLaReserva() {
		//arrange
		Reserva reserva = new Reserva();
		reserva.setId(1L);
		reserva.setId_cliente(2L);
		reserva.setId_auto(3L);
		reserva.setFechaInicio("27-10-2012");
		reserva.setFechaFinal("28-10-2012");
		
		//act
		when(reservaDao.save(reserva)).thenReturn(reserva);
		Reserva reservaCreada = reservaService.crearReserva(reserva);
		
		//assert
		assertEquals(reservaCreada,reserva);
	}

	public void listarReservaClienteIngresaIdClienteYFechaInicioRetornaIdAutoFechaInicioYFechaFin(){
		//arrange
		List<Reserva> reservas = new ArrayList<Reserva>();
		Reserva reserva = new Reserva();
		reserva.setId_cliente(1L);
		reserva.setFechaInicio("17-05-2017");
		
		//act
		when(reservaDao.save(reserva)).thenReturn(reserva);
		when(reservaDao.findAll()).thenReturn(reservas);
		ArrayList<Reserva> resultado = reservaService.obtenerTodos();
		
		//assert
		assertEquals(resultado,reservas);
		
		
	}
}
