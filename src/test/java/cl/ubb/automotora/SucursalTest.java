package cl.ubb.automotora;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.CategoriaClienteDao;
import cl.ubb.dao.SucursalDao;
import cl.ubb.model.Sucursal;
import cl.ubb.service.SucursalService;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SucursalTest {
	@Mock
	private SucursalDao sucursalDao;
	@InjectMocks
	private SucursalService sucursalService;
	
	
	@Test
	public void listarSucursalesRetornandoListaDeSucursales() {
		//arrange
		List<Sucursal> sucursales = new ArrayList<Sucursal>();
		
		//act
		when(sucursalDao.findAll()).thenReturn(sucursales);
		ArrayList<Sucursal> resultado = sucursalService.obtenerTodos();
		
		//assert
		assertNotNull(resultado);
		assertEquals(resultado,sucursales);
		
		
		
	}

}
