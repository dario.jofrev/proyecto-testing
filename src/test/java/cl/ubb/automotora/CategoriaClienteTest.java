package cl.ubb.automotora;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.CategoriaClienteDao;
import cl.ubb.model.CategoriaCliente;
import cl.ubb.service.CategoriaClienteService;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CategoriaClienteTest {
	@Mock
	private CategoriaClienteDao categoriaClienteDao;
	
	@InjectMocks
	private CategoriaClienteService categoriaClienteService;
	
	@Test
	public void listarLasCategoriasDeClientesRetornandoListaConIdentificadorYNombre() {
		//arrange
		List<CategoriaCliente> categoriaCliente = new ArrayList<CategoriaCliente>();
		
		//act
		when(categoriaClienteDao.findAll()).thenReturn(categoriaCliente);
		ArrayList<CategoriaCliente> resultado = categoriaClienteService.obtenerTodo();
		
		//assert
		assertNotNull(resultado);
		assertEquals(resultado,categoriaCliente);
		
	}

}
