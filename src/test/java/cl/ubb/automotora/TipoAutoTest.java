package cl.ubb.automotora;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.TipoAutoDao;
import cl.ubb.model.TipoAuto;
import cl.ubb.service.TipoAutoService;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TipoAutoTest {
	@Mock
	private TipoAutoDao tipoAutoDao;
	
	@InjectMocks
	private TipoAutoService tipoAutoService;
	
	@Test
	public void ingresarCategorialistarTiposDeAutoConDatos() {
		//assert
		TipoAuto tipoAuto = new TipoAuto();
		tipoAuto.setCategoria("Lujo");
		ArrayList<TipoAuto> listaTipoAutosPorCategoria = new ArrayList<TipoAuto>();
		listaTipoAutosPorCategoria.add(tipoAuto);
		ArrayList<TipoAuto> listaTipoAutosPorCategoriaRetornada = new ArrayList<TipoAuto>();
		
		//act
		when(tipoAutoDao.findByCategoria("Lujo")).thenReturn(listaTipoAutosPorCategoria);
		listaTipoAutosPorCategoriaRetornada = tipoAutoService.obtenerTiposAutosPorCategoria(tipoAuto.getCategoria());
		
		//assert
		assertNotNull(listaTipoAutosPorCategoriaRetornada);
		assertEquals(listaTipoAutosPorCategoriaRetornada,listaTipoAutosPorCategoria);
		
		
	}

}
