package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.TipoAutoDao;
import cl.ubb.model.TipoAuto;

public class TipoAutoService {
	private final TipoAutoDao tipoAutoDao;

	@Autowired
	public TipoAutoService(TipoAutoDao tipoAutoDao){
		this.tipoAutoDao=tipoAutoDao;
	}
	public ArrayList<TipoAuto> obtenerTiposAutosPorCategoria(String categoria) {
		ArrayList<TipoAuto> tiposAutos = (ArrayList<TipoAuto>)tipoAutoDao.findByCategoria(categoria);
		return tiposAutos;
	}

}
