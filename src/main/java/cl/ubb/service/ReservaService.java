package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;


import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Reserva;

public class ReservaService {
	
	private final ReservaDao reservaDao;
	@Autowired
	public ReservaService(ReservaDao reservaDao){
		this.reservaDao=reservaDao;
	}
	public Reserva crearReserva(Reserva reserva) {
		reservaDao.save(reserva);
		return reserva;
	}
	public ArrayList<Reserva> obtenerTodos() {
		ArrayList<Reserva> reservas = (ArrayList<Reserva>)reservaDao.findAll();
		return reservas;
	}
	

}
