package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.SucursalDao;
import cl.ubb.model.Sucursal;

public class SucursalService {
	private SucursalDao sucursalDao;
	
	@Autowired
	public SucursalService(SucursalDao sucursalDao){
		this.sucursalDao=sucursalDao;
	}
	public ArrayList<Sucursal> obtenerTodos() {
		ArrayList<Sucursal> sucursales = (ArrayList<Sucursal>)sucursalDao.findAll(); 
		return sucursales;
	}

}
