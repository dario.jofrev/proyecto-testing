package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.CategoriaClienteDao;
import cl.ubb.model.CategoriaCliente;

public class CategoriaClienteService {
	private CategoriaClienteDao categoriaClienteDao;
	
	@Autowired
	public CategoriaClienteService(CategoriaClienteDao clienteDao){
		this.categoriaClienteDao=clienteDao;
	}
	public ArrayList<CategoriaCliente> obtenerTodo() {
		ArrayList<CategoriaCliente> categoriasCliente= (ArrayList<CategoriaCliente>)categoriaClienteDao.findAll();
		return categoriasCliente;
	}

}
