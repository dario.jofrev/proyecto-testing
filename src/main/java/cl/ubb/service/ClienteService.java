package cl.ubb.service;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.ClienteDao;
import cl.ubb.model.Cliente;


public class ClienteService {
	private ClienteDao clienteDao;
	
	@Autowired
	public ClienteService(ClienteDao clienteDao){
		this.clienteDao=clienteDao;
	}
	
	public Cliente crearCliente(Cliente cliente) {
		clienteDao.save(cliente);
		return cliente;
	}

	public boolean buscarCliente(String rut) {
		return clienteDao.exists(rut);
	}

}
