package cl.ubb.service;

import cl.ubb.dao.EspecificacionAutoDao;
import cl.ubb.model.EspecificacionAuto;

public class EspecificacionAutoService {
	
	private final EspecificacionAutoDao especificacionAutoDao;
	public EspecificacionAutoService(EspecificacionAutoDao especificacionAutoDao){
		this.especificacionAutoDao=especificacionAutoDao;
	}
	
	
	public EspecificacionAuto obtenerEspecificacionAuto(long id) {
		EspecificacionAuto especificacionAuto = new EspecificacionAuto();
		especificacionAuto= especificacionAutoDao.findOne(id);
		
		return especificacionAuto;
	}

}
