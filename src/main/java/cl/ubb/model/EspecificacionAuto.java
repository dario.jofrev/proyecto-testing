package cl.ubb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EspecificacionAuto {
	@GeneratedValue
	@Id
	private long id;
	private String marca;
	private String modelo;
	private String anio;
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public void setId(long id) {
		this.id=id;
		
	}
	public long getId() {
		return id;
	}
}
