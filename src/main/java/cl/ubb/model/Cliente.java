package cl.ubb.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
@Entity
public class Cliente {
	
	String rut;
	String nombre;
	String numeroTel;
	String email;
	
	public Cliente(){
		
	}
	
	public String getRut(){
		return rut;
	}
	public void setRut(String rut){
		this.rut=rut;
	}
	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNumeroTel(){
		return numeroTel;
	}
	public void setNumeroTel(String numeroTel){
		this.numeroTel=numeroTel;
	}
	public String getEmail(){
		return email;
	}
	public void setEmail(String email){
		this.email=email;
	}
	
}
