package cl.ubb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TipoAuto {
	@Id
	@GeneratedValue
	private long id;
	private String nombre;
	private String categoria;
	private String tipoTransmision;
	private String tipoCombustible;
	private boolean aireAcondicionado;
	private boolean bags;
	private int pasajeros;
	private int precioDiario;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoTransmision() {
		return tipoTransmision;
	}
	public void setTipoTransmision(String tipoTransmision) {
		this.tipoTransmision = tipoTransmision;
	}
	public boolean isAireAcondicionado() {
		return aireAcondicionado;
	}
	public void setAireAcondicionado(boolean aireAcondicionado) {
		this.aireAcondicionado = aireAcondicionado;
	}
	public boolean isBags() {
		return bags;
	}
	public void setBags(boolean bags) {
		this.bags = bags;
	}
	public String getTipoCombustible() {
		return tipoCombustible;
	}
	public void setTipoCombustible(String tipoCombustible) {
		this.tipoCombustible = tipoCombustible;
	}
	public int getPasajeros() {
		return pasajeros;
	}
	public void setPasajeros(int pasajeros) {
		this.pasajeros = pasajeros;
	}
	public int getPrecioDiario() {
		return precioDiario;
	}
	public void setPrecioDiario(int precioDiario) {
		this.precioDiario = precioDiario;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	
}
