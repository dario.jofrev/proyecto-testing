package cl.ubb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Reserva {
	@Id
	@GeneratedValue
	private long id;
	private long id_cliente;
	private long id_auto;
	private String fechaInicio;
	private String fechaFinal;
	public long getId_auto() {
		return id_auto;
	}
	public void setId_auto(long id_auto) {
		this.id_auto = id_auto;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public void setId(long id) {
		this.id=id;
	}
	public long getId(){
		return id;
	}
	public long getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(long id_cliente) {
		this.id_cliente = id_cliente;
	}
}
