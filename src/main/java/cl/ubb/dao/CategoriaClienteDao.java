package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.CategoriaCliente;

public interface CategoriaClienteDao extends CrudRepository<CategoriaCliente, Long> {

}
