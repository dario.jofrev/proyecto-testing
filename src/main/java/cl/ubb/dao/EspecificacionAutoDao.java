package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.EspecificacionAuto;

public interface EspecificacionAutoDao extends CrudRepository<EspecificacionAuto,Long>{

}
