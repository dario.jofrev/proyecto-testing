package cl.ubb.dao;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Cliente;
public interface ClienteDao  extends CrudRepository<Cliente, String>{

}
