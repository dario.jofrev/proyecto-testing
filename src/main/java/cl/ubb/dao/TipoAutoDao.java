package cl.ubb.dao;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.TipoAuto;

public interface TipoAutoDao extends CrudRepository<TipoAuto,Long>{
	public ArrayList<TipoAuto> findByCategoria(String categoria);

}
